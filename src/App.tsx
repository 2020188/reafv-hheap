import './App.css';
import React, { useEffect } from 'react';
import FlappyBird from './flappy_bird/FlappyBird';
import Game2 from './game2/Game2';


function App() {

	return (
		<div className="background">
			<div className="gameWindow">
				{/* <Game2></Game2> */}
				<FlappyBird></FlappyBird>
			</div>
		</div>
	)
}


export default App