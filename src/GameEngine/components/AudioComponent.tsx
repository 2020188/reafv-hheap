import { GameComponent } from "./GameComponent";

export class AudioComponent extends GameComponent {
    public audio!: HTMLAudioElement;
    private link: string;

    public constructor(link : string) {
        super();
        this.link = link;
        this.audio = new Audio(link);
    }

    public getAudio(): HTMLAudioElement {
        return this.audio;
    }

    public playAudio(){
        this.audio.play();
    }

}