import { GameComponent } from "./GameComponent";

/**
 * Class defines a collision hit-box.
 */
export class CollisionComponent extends GameComponent {
	public width : number;
	public height : number;
	public time : number[];
	public collides : CollisionComponent[];
	
	/**
	 * Constructs a new collision component.
	 * @param width - number
	 * @param height - number
	 */
	public constructor(width : number, height : number) {
		super();
		this.width = width;
		this.height = height;
		this.time = [];
		this.collides = [];
	}
}