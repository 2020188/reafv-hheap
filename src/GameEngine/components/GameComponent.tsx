import { Entity } from "../core/Entity";

/**
 * Interface defines an abstract game component.
 */
export abstract class GameComponent { // TODO: abstract ok?
    protected parent: Entity | undefined;

    public constructor(public enabled: boolean = true){

    }

    public setParent(parent:Entity) {
        this.parent = parent;
    }

    public getParent(): Entity | undefined{
        return this.parent;
    }
}