import { Vector2 } from "../utils/Vector2";
import { GameComponent } from "./GameComponent";

/**
 * Class defines a physics component.
 */
export class PhysicsComponent extends GameComponent {
	
	/**
	 * Constructs a new physics component.
	 */
	public constructor(public mass?: number, public drag?: number, public gravity?: Vector2) {
		super();
	}
}