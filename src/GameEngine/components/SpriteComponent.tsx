import React from "react";
import { GameComponent } from "./GameComponent";

/**
 * Class defines a sprite.
 */
export class SpriteComponent extends GameComponent {
	public image!: JSX.Element;
	
	/**
	 * Constructs a new sprite component.
	 * @param link - string - Link to image
	 */
	public constructor(public link : string, public width: number, public height: number, public z?:number) {
		super();

		//TODO: x and y values added here for testing purposes.
		//TODO: Height is hardcoded temporarily for testing purposes.
	}

	public getImage(): JSX.Element {
		if(this.parent == undefined){
			//TODO: do something
		}
		this.image = <img src={this.link} width={this.width} height={this.height} key={this.getParent()?.name}
		style={{
			position:"absolute", 
			transform: this.parent?.transform.getCSSTransform(),
			zIndex: this.z
		}}></img>;
		return this.image;
	}
}