import React from "react";
import { GameComponent } from "./GameComponent";

export class UITextComponent extends GameComponent{

    public constructor(public text: string){
        super();
    }

    public getText(): JSX.Element{
        return <p
        style={{
			position:"absolute", 
			transform: this.parent?.transform.getCSSTransform(),
			zIndex: 20
		}}>
            {this.text}
        </p>
    }
}