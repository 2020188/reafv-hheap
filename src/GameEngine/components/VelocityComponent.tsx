import { Vector2 } from "../utils/Vector2";
import { GameComponent } from "./GameComponent";

/**
 * Class defines a velocity vector.
 */
export class VelocityComponent extends GameComponent {
	/**
	 * Constructs a new velocity component.
	 * @param vector - Vector2
	 */
	public constructor(public vector:Vector2, public maxVector?:Vector2) {
		super();
	}

	public addVector(vec: Vector2){
		this.vector = Vector2.add(vec, this.vector);
	}

	public setVector(newVector: Vector2){
		this.vector = newVector;
	}

	public setX(newX: number){
		this.vector.x = newX;
	}
	public setY(newY: number){
		this.vector.y = newY;
	}
}