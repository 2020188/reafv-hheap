import { ClassType, Component, Fragment, useEffect, useMemo, useState } from "react";
import {GameComponent} from "../components/GameComponent";
import { Vector2 } from "../utils/Vector2";
import { Transform, transformProps, useTransform } from "./Transform";

type EntityProps = {
	name: string,
	components?: GameComponent[],
	transform?: transformProps,
	enabled?: boolean,
}

export type Entity = {
	name: string,
	enabled: boolean
	transform: Transform,

	components: Readonly<GameComponent[]>,
	addComponent(comp: GameComponent): void,
	getComponent<Tcomp extends GameComponent>(compType: new(...params: any[]) => Tcomp):Tcomp | null 
}

export function useReactEntity(props: EntityProps):  Entity{
	const transform = useTransform(props.transform);
	const [components, setComponents] = useState<GameComponent[]>(props.components || []);
	const [name, setName] = useState(props.name);
	const [enabled, setEnabled] = useState(props.enabled || true)

	const entity: Entity = useMemo(() => {
		const tempEntity: Entity = {
			name,
			enabled: enabled,
			transform,

			components: components,
			addComponent: (comp) => {
				comp.setParent(tempEntity); // TODO: er hetta okay?
				setComponents(components => [...components, comp]);
			},
			getComponent: <Tcomp extends GameComponent>(compType: new (...params: any[]) => Tcomp) => {
				const found = components.find(comp => comp instanceof compType);
				return found ? found as Tcomp : null //TODO: kundi havt hetta sum undefined
			}
		};
		components.forEach((comp) => comp.setParent(tempEntity));
		return tempEntity;
	}, [transform, components, name]);

	
	return entity
}