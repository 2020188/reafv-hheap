import React, { PropsWithChildren, useCallback, useEffect, useReducer, useRef, useState } from "react";
import Renderer from "./Renderer";
import { Entity } from "./Entity";
import DefaultTimer from "./GameLoop";

type GameEngineProps = {
    systems?: any[],
    entities: Entity[],
    timer: DefaultTimer,
} & typeof defaultEngineProps;

const defaultEngineProps = {
    entities: [] as Entity[],
    running: false,
    renderer: Renderer,
    timer: new DefaultTimer(),
}
GameEngine.defaultProps = defaultEngineProps;


let previousTime: number | null;
let previousDelta: number | null;
let input: object[] = [];
let events: Event[] = [];

type reducerAction = {
    system: (entities: Entity[], args: any) => Entity[],
    args: any
}

function entityReducer(state: Entity[], action: reducerAction): Entity[] {
    const { system, args } = action;
    const newstate = system(state, args);
    const tempSuperShitHack: Entity[] = [];
    newstate.forEach((entity) => {
        tempSuperShitHack.push(entity.components[0].getParent()!)
    });

    return tempSuperShitHack;
}


const inputEvents = `onClick onContextMenu onDoubleClick onDrag onDragEnd onDragEnter onDragExit onDragLeave onDragOver onDragStart onDrop onMouseDown onMouseEnter onMouseLeave onMouseMove onMouseOut onMouseOver onMouseUp onWheel onTouchCancel onTouchEnd onTouchMove onTouchStart onKeyDown onKeyPress onKeyUp`;

export default function GameEngine(props: PropsWithChildren<GameEngineProps>) {
    const [entities, dispatchEntities] = useReducer(entityReducer, props.entities);
    const [running, setRunning] = useState(props.running);
    const container = useRef<HTMLInputElement | null>(null);

    // hetta er tað sum hendir í okkara game loop. Hetta verður "subscribe'a" á okkara time'ara
    const updateHandler = useCallback((currentTime: number) => {
        //tað sum er broytt millum loops
        const args = { 
            dispatchEvent: dispatchEvent,
            events: events,
            input: input,
            time: {
                current: currentTime,
                previous: previousTime,
                delta: previousTime ? (currentTime - previousTime) : null, //if no previousTime then set delta to null
                previousDelta: previousDelta
            }
        }

        //loop'a gjøgnum øll systems
        props.systems?.forEach((system) => {
            const action = { system: system, args: args }
            dispatchEntities(action);
        });
        
        input = [];
        previousTime = currentTime;
        previousDelta = args.time.delta;
    }, [entities, input, events, previousTime, previousDelta]);


    //useEffect er kalla hvørjarferð hesin component blívur rendered
    useEffect(() => {
        props.timer.subscribe(updateHandler);
        return function cleanup() {
            props.timer.unsubscribe(updateHandler);
        }
    });

    useEffect(() => {
        setRunning(true);
    }, [])

    // tá running er broytt; start ella stop game loopinum.
    useEffect(() => {
        const start = () => {
            clear();
            dispatchEvent(new Event("started"));
            container.current!.focus();
            props.timer.start();
        }

        const stop = () => {
            dispatchEvent(new Event("stopped"));
            props.timer.stop();
        }

        const clear = () => {
            input = [];
            events = [];
            previousTime = null;
            previousDelta = null;
        }

        running ? start() : stop();
    }, [running]);

    const dispatchEvent = (event: Event) => {
        setTimeout(() => {
            events.push(event);
            //TODO: custom event handlara frá props
        }, 0);
    }

    const inputHandlers = inputEvents
        .split(" ")
        .map(name => ({
            name,
            handler: (payload: any) => {
                payload.persist();
                input.push({ name, payload });
            }
        }))
        .reduce((acc, val) => {
            (acc as any)[val.name] = val.handler; // TODO: as any er ikki gott
            return acc;
        }, {});


    return (
        <div
            ref={container}
            style={{ ...cssStyle.container }}
            tabIndex={0} //neyðugt fyri at fáa input frá keyboard.
            {...inputHandlers}
        >
            {props.renderer(entities)}
            {props.children}
        </div>
    )
}


const cssStyle = {
    container: {
        flex: 1,
        outline: "none",
    }
};