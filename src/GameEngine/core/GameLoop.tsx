

export default class DefaultTimer {
  constructor() {
    this.subscribers = [];
    this.loopId = null;
  }
  private loopId: number | null;
  private subscribers: any[];

  loop = (time: number | null) => {
    if (this.loopId) {
      this.subscribers.forEach(callback => {
        callback(time);
      });
    }

    this.loopId = requestAnimationFrame(this.loop);
  };

  start() {
    if (!this.loopId) {
      this.loop(null);
    }
  }

  stop() {
    if (this.loopId) {
      cancelAnimationFrame(this.loopId);
      this.loopId = null;
    }
  }

  subscribe(callback: any) {
    if (this.subscribers.indexOf(callback) === -1)
      this.subscribers.push(callback);
  }

  unsubscribe(callback: any) {
    this.subscribers = this.subscribers.filter(s => s !== callback)
  }
}