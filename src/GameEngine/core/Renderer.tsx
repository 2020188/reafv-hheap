import { GameComponent } from '../components/GameComponent';
import { SpriteComponent } from '../components/SpriteComponent';
import { UITextComponent } from '../components/UITextComponent';
import { Entity } from './Entity';
import { all, allComponents, allEnabled } from '../utils';


export default (entities: Entity[]) => {
	const spriteEntities = allEnabled(entities, [SpriteComponent]);
	const allSprites = allComponents(spriteEntities, SpriteComponent);
	//make an array of all enabled sprite images
	const images = allSprites.map((sprite)=>{
		if(!sprite.enabled){
			return;
		}
		return sprite.getImage();
	});

	//text rendering

	const textEntities = allEnabled(entities, [UITextComponent]);
	const allUIText = allComponents(textEntities, UITextComponent)
	const text = allUIText.map((UIText)=>{
		if(!UIText.enabled){
			return;
		}
		return UIText.getText();
	});

	return [...images, ...text]
}