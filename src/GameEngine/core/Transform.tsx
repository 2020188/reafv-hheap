import { useEffect, useReducer, useState } from "react";
import { Vector2 } from "../utils/Vector2";

export type Transform = {
    position: Readonly<Vector2>,
    setPosition(value: Vector2): void,

    translate(pos: Vector2): void

    getPosX(): number,
    getPosY(): number,

    rotation: number,
    setRotation(value: number): void,
    rotate(angle: number): void,

    getCSSTransform(): string,
}

export type transformProps = {
    position?: Vector2
    rotation?: number
}

enum ActionKind {
    Set = "set",
    Translate = "translate"
}

function positionReducer(position: Vector2, action: { type: ActionKind, newVector: Vector2 }): Vector2 {
    let { newVector } = action;
    const { type } = action;
    switch (type) {
        case ActionKind.Set:
            return ({...position, ...newVector});
        case ActionKind.Translate:
            newVector = Vector2.add(newVector, position);
            return ({...position, ...newVector});
        default:
            return position;
    }
}

export function useTransform(props?: transformProps): Transform { //TODO: rotation, scale
    // const [position, setPosition] = useState<Vector2>(props?.position || new Vector2(0,0));
    const [position, dispatchPosition] = useReducer(positionReducer, (props?.position || new Vector2(0, 0)))
    const [rotation, setRotation] = useState(props?.rotation || 0);

    const translate = (pos: Vector2) => {
        dispatchPosition({ type: ActionKind.Translate, newVector: pos });
        // setPosition((current) => {

        //     const newVec = Vector2.add(pos, current);
        //     return newVec;

        // })
    }

    const rotate = (angle: number) =>{
        setRotation((current)=> current + angle);
    }
    
    const getCSSTransform = () =>{
        return "translate(" + position.x + "px, " + position.y + "px) rotate("+ rotation + "deg)"
    }

    const setPosition = (newPos: Vector2) => {
        dispatchPosition({ type: ActionKind.Set, newVector: newPos });
    }

    const getPosX = () => { return position.x; };
    const getPosY = () => { return position.y; };

    return {
        position,
        setPosition,
        translate,

        getPosX,
        getPosY,

        rotation,
        setRotation,
        rotate,

        getCSSTransform,
    }
}