/**
 * Class defines a debugger.
 */
export class Debugger {
	
	/**
	 * Prints an object to the debug console.
	 * @param msg - any
	 */
	public static log(msg: any): void {
		console.log(msg);
	}
}