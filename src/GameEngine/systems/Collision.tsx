import { CollisionComponent } from "../components/CollisionComponent";
import { VelocityComponent } from "../components/VelocityComponent";
import { all } from "../utils";
import { Vector2 } from "../utils/Vector2";
import { Entity } from "../core/Entity";
import { advancedBoxCollision, isCollideAble } from "../utils/CollisionFunctions";

/**
 * 
 */
export function Collision(entities: Entity[]) {
	
	// Collision detection
	const collEntities = all(entities, [CollisionComponent]);
	const movCollEntities = all(collEntities, [VelocityComponent]);
	movCollEntities
		.forEach(entity1 => {
			collEntities.forEach(entity2 => {
			if (entity1 === entity2) return;
			
			
			const collision1 = entity1.getComponent(CollisionComponent);
			const collision2 = entity2.getComponent(CollisionComponent);
			
			const velocity1 = entity1.getComponent(VelocityComponent);
			let velocity2 = entity2.getComponent(VelocityComponent);
			
			if (velocity2 == null) velocity2 = new VelocityComponent(new Vector2(0, 0));
			
			const box1 = {
				position : {
					x : collision1!.getParent()!.transform.getPosX(),
					y : collision1!.getParent()!.transform.getPosY()
				},
				area : {
					width : collision1!.width,
					height : collision1!.height
				},
				velocity : {
					x : velocity1!.vector.x,
					y : velocity1!.vector.y
				}
			};
			const box2 = {
				position : {
					x : collision2!.getParent()!.transform.getPosX(),
					y : collision2!.getParent()!.transform.getPosY()
				},
				area : {
					width : collision2!.width,
					height : collision2!.height
				},
				velocity : {
					x : velocity2!.vector.x,
					y : velocity2!.vector.y
				}
			};
			
			let result;
			if (!isCollideAble(box1, box2)) {
				result = Number.POSITIVE_INFINITY;
			} else {
				result = advancedBoxCollision(box1, box2);
			}
			
			if (result < 1) {
				collision1!.collides.push(collision2!);
				collision1!.time.push(result);
			}
		})
	});
	return entities;
}