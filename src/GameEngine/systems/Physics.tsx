import { PhysicsComponent } from "../components/PhysicsComponent";
import { VelocityComponent } from "../components/VelocityComponent";
import { Entity } from "../core/Entity";
import { all } from "../utils";
import { gravity, drag } from "../utils/PhysicsFunctions";
import { Vector2 } from "../utils/Vector2";

export function Physics(entities : Entity[]) {
	
	// Collision detection
	const physicsEntities = all(entities, [PhysicsComponent, VelocityComponent]);
	physicsEntities
		.forEach(entity => {
			const physicsComponent = entity.getComponent(PhysicsComponent);
			const velocityComponent = entity.getComponent(VelocityComponent);
			
			if (physicsComponent?.gravity != null) velocityComponent?.setVector(gravity(velocityComponent.vector, physicsComponent.gravity));
			if (physicsComponent?.mass != null && physicsComponent?.drag != null) velocityComponent?.setVector(drag(velocityComponent.vector, physicsComponent.drag, physicsComponent.mass));
		}
	);
	
	return entities;
}