import { CollisionComponent } from "../components/CollisionComponent";
import { VelocityComponent } from "../components/VelocityComponent";
import { all } from "../utils";
import { boxCollision } from "../utils/CollisionFunctions";
import { Entity } from "../core/Entity";

/**
 * 
 */
export const SimpleCollision = (entities: Entity[]) => {
	
	// Collision detection
	const collEntities = all(entities, [CollisionComponent]);
	const movCollEntities = all(collEntities, [VelocityComponent]);
	movCollEntities
		.forEach(entity1 => {
			collEntities.forEach(entity2 => {
				if (entity1 === entity2) return;
				
				const collision1 = entity1.getComponent(CollisionComponent);
				const collision2 = entity2.getComponent(CollisionComponent);
				
				const box1 = {
					x1 : collision1!.getParent()!.transform.getPosX(),
					y1 : collision1!.getParent()!.transform.getPosY(),
					x2 : collision1!.getParent()!.transform.getPosX() + collision1!.width,
					y2 : collision1!.getParent()!.transform.getPosY() + collision1!.height,
				}
				const box2 = {
					x1 : collision2!.getParent()!.transform.getPosX(),
					y1 : collision2!.getParent()!.transform.getPosY(),
					x2 : collision2!.getParent()!.transform.getPosX() + collision2!.width,
					y2 : collision2!.getParent()!.transform.getPosY() + collision2!.height,
				}
				
				const result = boxCollision(box1, box2);
				
				if (result) {
					collision1!.collides.push(collision2!);
				}
			})
		}
	);
	return entities;
}