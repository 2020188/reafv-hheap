/**
 * Uses the Axis-Aligned Bounding Box collision method to check if two boxes are colliding.
 * @param box1 - x1 and y1 denominating one corner of the box, while x2 and y2 denominate the opposite corner
 * @param box2 - x1 and y1 denominate the corner matching to the corner in box1, same goes for x2 and y2
 * @returns boolean
 */
export const boxCollision = (box1 : {x1 : number, y1 : number, x2 : number, y2 : number}, box2 : {x1 : number, y1 : number, x2 : number, y2 : number}) : boolean => {
	return (
		box1.x1 < box2.x2 &&
		box2.x1 < box1.x2 &&
		box1.y1 < box2.y2 &&
		box2.y1 < box1.y2
	);
}

/**
 * Compares the radii of two circles to the distance between their center points to check if they are colliding.
 * @param circle1 - circle with position and radius
 * @param circle2 - circle with position and radius
 * @returns boolean
 */
export const circleCollision = (circle1 : {position : {x : number, y : number}, area : {radius : number}}, circle2 : {position : {x : number, y : number}, area : {radius : number}}) : boolean => {
	
	// Delta x and y
	const dx = (circle1.position.x + circle1.area.radius) - (circle2.position.x + circle2.area.radius);
	const dy = (circle1.position.y + circle1.area.radius) - (circle2.position.y + circle2.area.radius);
	
	const distance = Math.sqrt(dx * dx + dy * dy);
	
	if (distance < circle1.area.radius + circle2.area.radius) return true;
	return false;
}

/**
 * Uses combination of box- and circle-collision to determine if a circle is colliding with a box.
 * @param circle - circle with position and radius
 * @param box - box with position and dimensions
 * @returns boolean
 */
export const circleBoxCollision = (circle : {position : {x : number, y : number}, area : {radius : number}}, box : {position : {x : number, y : number}, area : {width : number, height : number}}) : boolean => {
	
	return (
		// Checks lower left corner
		circleCollision(circle, {
			position : {x : box.position.x, y : box.position.y},
			area : {radius : 0}
		}) ||
		
		// Checks lower right corner
		circleCollision(circle, {
			position : {x : box.position.x + box.area.width, y : box.position.y},
			area : {radius : 0}
		}) ||
		
		// Checks upper left corner
		circleCollision(circle, {
			position : {x : box.position.x, y : box.position.y + box.area.height},
			area : {radius : 0}
		}) ||
		
		// Checks upper right corner
		circleCollision(circle, {
			position : {x : box.position.x + box.area.width, y : box.position.y + box.area.height},
			area : {radius : 0}
		}) ||
		
		// Checks extended horizontal box
		boxCollision({
			x1 : box.position.x - circle.area.radius, 
			y1 : box.position.y,
			x2 : box.position.x + box.area.width + circle.area.radius,
			y2 : box.position.y + box.area.height
		}, {
			x1 : circle.position.x + circle.area.radius,
			y1 : circle.position.y + circle.area.radius,
			x2 : circle.position.x + circle.area.radius,
			y2 : circle.position.y + circle.area.radius,
		}) ||
		
		// Checks extended vertical box
		boxCollision({
			x1 : box.position.x, 
			y1 : box.position.y - circle.area.radius,
			x2 : box.position.x + box.area.width,
			y2 : box.position.y + box.area.height + circle.area.radius
		}, {
			x1 : circle.position.x + circle.area.radius,
			y1 : circle.position.y + circle.area.radius,
			x2 : circle.position.x + circle.area.radius,
			y2 : circle.position.y + circle.area.radius,
		})
	)
}

/**
 * Checks if there is a possibility of two boxes to collide within a tick.
 * @param box1 - box with position, dimensions, and velocity
 * @param box2 - box with position, dimensions, and velocity
 * @returns boolean
 */
export const isCollideAble = (box1 : {position : {x : number, y : number}, area : {width : number, height : number}, velocity : {x : number, y : number}}, box2 : {position : {x : number, y : number}, area : {width : number, height : number}, velocity : {x : number, y : number}}) : boolean => {
	return boxCollision(
		{
			x1 : box1.velocity.x < 0 ? box1.position.x + box1.velocity.x : box1.position.x,
			y1 : box1.velocity.y < 0 ? box1.position.y + box1.velocity.y : box1.position.y,
			x2 : box1.velocity.x < 0 ? box1.position.x + box1.area.width : box1.position.x + box1.area.width + box1.velocity.x,
			y2 : box1.velocity.y < 0 ? box1.position.y + box1.area.height : box1.position.y + box1.area.height + box1.velocity.y,
		},
		{
			x1 : box2.velocity.x < 0 ? box2.position.x + box2.velocity.x : box2.position.x,
			y1 : box2.velocity.y < 0 ? box2.position.y + box2.velocity.y : box2.position.y,
			x2 : box2.velocity.x < 0 ? box2.position.x + box2.area.width : box2.position.x + box2.area.width + box2.velocity.x,
			y2 : box2.velocity.y < 0 ? box2.position.y + box2.area.height : box2.position.y + box2.area.height + box2.velocity.y,
		}
	)
}

/**
 * Checks collision of two 1D parametric equation, and returns the time of collision.
 * @param equation1 - parametric equation with position and vector
 * @param equation2 - parametric equation with position and vector
 * @returns ticks until collision
 */
export const oneDimensionalCollision = (equation1 : {position : number, velocity : number}, equation2 : {position : number, velocity : number}) : number => {
	if (equation1.position == equation2.position) return 0;
	if (equation1.velocity == equation2.velocity) return Number.POSITIVE_INFINITY;
	return (equation1.position - equation2.position) / (equation2.velocity - equation1.velocity);
}

/**
 * Checks collision of two 2D parametric equation, and returns the time of collision.
 * @param equation1 - parametric equation with position and vector
 * @param equation2 - parametric equation with position and vector
 * @returns ticks until collision
 */
export const parametricCollision = (equation1 : {position : {x : number, y : number}, vector : {x : number, y : number}}, equation2 : {position : {x : number, y : number}, vector : {x : number, y : number}}) : number => {
	
	// One dimensional y vector
	if (equation1.vector.x == 0 && equation2.vector.x == 0) {
		if (equation1.position.x != equation2.position.x) return Number.POSITIVE_INFINITY;
		
		return oneDimensionalCollision({
			position : equation1.position.y,
			velocity : equation1.vector.y
		}, {
			position : equation2.position.y,
			velocity : equation2.vector.y
		});
	}
	
	// One dimensional x vector
	if (equation1.vector.y == 0 && equation2.vector.y == 0 && equation1.position.y == equation2.position.y) {
		if (equation1.position.y != equation2.position.y) return Number.POSITIVE_INFINITY;
		
		return oneDimensionalCollision({
			position : equation1.position.x,
			velocity : equation1.vector.x
		}, {
			position : equation2.position.x,
			velocity : equation2.vector.x
		});
	}
	
	// Parallel vectors
	if (equation1.vector.x * equation2.vector.y == equation2.vector.x * equation1.vector.y) return Number.POSITIVE_INFINITY;
	
	// Normal vectors
	if (equation2.vector.x == 0) {
		return (
			equation2.vector.y * (equation1.position.x - equation2.position.x) +
			equation2.vector.x * (equation2.position.y - equation1.position.y)
			) / (
			equation2.vector.x * equation1.vector.y -
			equation1.vector.x * equation2.vector.y
		);
	}
	
	return (
		equation1.vector.y * (equation2.position.x - equation1.position.x) +
		equation1.vector.x * (equation1.position.y - equation2.position.y)
		) / (
		equation1.vector.x * equation2.vector.y -
		equation2.vector.x * equation1.vector.y
	);
}

/**
 * Checks collision between a parametric equation and a box, and returns the time of collision.
 * @param equation - parametric equation with position and vector
 * @param box - box with position, dimensions, and velocity
 * @returns ticks until collision
 */
export const parametricBoxCollision = (equation : {position : {x : number, y : number}, velocity : {x : number, y : number}}, box : {position : {x : number, y : number}, area : {width : number, height : number}, velocity : {x : number, y : number}}) => {
	// x1 and y1 denominating one corner of the box,
	// while x2 and y2 denominate the opposite corner
	
	const x1 = oneDimensionalCollision({position : equation.position.x, velocity : equation.velocity.x}, {position : box.position.x + box.area.width, velocity : box.velocity.x});
	const y1 = oneDimensionalCollision({position : equation.position.y, velocity : equation.velocity.y}, {position : box.position.y + box.area.height, velocity : box.velocity.y});
	
	const x2 = oneDimensionalCollision({position : equation.position.x, velocity : equation.velocity.x}, {position : box.position.x, velocity : box.velocity.x});
	const y2 = oneDimensionalCollision({position : equation.position.y, velocity : equation.velocity.y}, {position : box.position.y, velocity : box.velocity.y});
	if (x2 <= 0 && y2 <= 0) return 0;
	
	if (x1 < y1 && y1 < x2) return y1;
	if (y1 < x1 && x1 < y2) return x1;
	
	return Number.POSITIVE_INFINITY;
}

/**
 * Uses a variation of the Axis-Aligned Bounding Box collision method to check if and when two boxes are colliding.
 * @param box1 - box with position, dimensions, and velocity
 * @param box2 - box with position, dimensions, and velocity
 * @returns ticks until collision
 */
export const advancedBoxCollision = (box1 : {position : {x : number, y : number}, area : {width : number, height : number}, velocity : {x : number, y : number}}, box2 : {position : {x : number, y : number}, area : {width : number, height : number}, velocity : {x : number, y : number}}) : number => {
	const case1 = parametricBoxCollision(
		{
			position : {
				x : box1.position.x,
				y : box1.position.y
			},
			velocity : {
				x : box1.velocity.x,
				y : box1.velocity.y
			}
		}, {
			position : {
				x : box2.position.x,
				y : box2.position.y
			},
			area : {
				width : box2.area.width,
				height : box2.area.height
			},
			velocity : {
				x : box2.velocity.x,
				y : box2.velocity.y
			}
		}
	);
	const case2 = parametricBoxCollision(
		{
			position : {
				x : box1.position.x + box1.area.width,
				y : box1.position.y + box1.area.height
			},
			velocity : {
				x : box1.velocity.x,
				y : box1.velocity.y
			}
		}, {
			position : {
				x : box2.position.x,
				y : box2.position.y
			},
			area : {
				width : box2.area.width,
				height : box2.area.height
			},
			velocity : {
				x : box2.velocity.x,
				y : box2.velocity.y
			}
		}
	);
	if (case1 < 0 && case2 < 0) return Number.POSITIVE_INFINITY;
	if (case1 < 0 || case2 < 0) return case1 < case2 ? case2 : case1;
	return case1 < case2 ? case1 : case2;
}