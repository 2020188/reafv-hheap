import { Vector2 } from "./Vector2"

export const gravity = (vector: Vector2, gravity: Vector2) : Vector2 => {
	return Vector2.add(vector, gravity);
}

export const drag = (vector: Vector2, drag: number, mass: number) : Vector2 => {
	const dragVector = new Vector2(0, -vector.y * ((drag/mass) - 1));
	return Vector2.add(vector, dragVector);
}