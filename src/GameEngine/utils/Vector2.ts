
export class Vector2{

    constructor(public x: number, public y: number){
    }

    // public add(rhs:Vector2){
    //     this.x += rhs.x;
    //     this.y += rhs.y;
    //     // return new Vector2(this.x + rhs.x, this.y + rhs.y);
    //     return this;
    // }

    public static add(lhs:Vector2, rhs:Vector2){
       
        return new Vector2(lhs.x + rhs.x, lhs.y + rhs.y);
    }

    
}

