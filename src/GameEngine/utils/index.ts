import { GameComponent } from "../components/GameComponent"
import { Entity } from "../core/Entity"

// planin er at sleppa undan at skriva kotu sum hetta. hetta dømi er tiki frá physics
    // entities.filter(entity =>
	// entity.components.some(component => component instanceof PhysicsComponent) &&
	// entity.components.some(component => component instanceof VelocityComponent))

    // eg skal kunna skriva allComps(entities, okkurt til at at boða hvørjar comps eg vil hava)
    // veit ikki heilt hvussu seinna argumenti skal skrivast enn. kanska (entities, comp => comp instanceof PhysicsComponent)?

    // return skal vera allir entities sum hava components sum match'a predicate.

const all = (entities: Entity[], comps: Array<(new(...params: any[]) => GameComponent)>) => {
    return entities.filter(entity =>{
        return comps.every((component)=>{
            const result = entity.getComponent(component) ? true : false;
            return result;
        } ) ? true : false;
    });
}
// sama sum all uttan tað at hendan hevur eitt early return um entity ikki er enabled.
const allEnabled = (entities: Entity[], comps: Array<(new(...params: any[]) => GameComponent)>) => {
    return entities.filter(entity =>{
        if(!entity.enabled){
            return false;
        }
        return comps.every((component)=>{
            const result = entity.getComponent(component) ? true : false;
            return result;
        } ) ? true : false;
    });
}

const allComponents = <Tcomp extends GameComponent>(entities: Entity[], comp: (new(...params: any[]) => Tcomp)) => {
    const allEnts = all(entities, [comp]);
    let allComps: GameComponent[] = [];
    //loop'a gjøgnum øll components á hvørjum entity
    allEnts.forEach((entity)=> {
        const entComps = entity.components.filter((component) => component instanceof comp);
        //um har var eitt ella fleiri matches so "concatinate" allComps og entComps
        if(entComps){
            allComps = [...allComps, ...entComps];
        }
    });
    return allComps as Tcomp[];
}


export{
    all,
    allEnabled,
    allComponents
}