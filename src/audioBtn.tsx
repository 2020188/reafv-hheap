import React, {useState, useEffect} from 'react';

type playerProps = {
    url: string;
}

export function  Player(props: playerProps){
    const[audio] = useState(new Audio(props.url));
    const[playing, setPlaying] = useState(false);

    useEffect(() => {
        playing ? audio.play() : audio.pause();
    }, [playing]);

    useEffect(()=> {
        audio.addEventListener('ended', () => setPlaying(false));
        return () => {
            audio.removeEventListener('ended', () => setPlaying(false));
        };
    }, []);
    return (
        <div>
            <button onClick={() => setPlaying(!playing) }>{playing ? "Pause" : "Play"}</button>
        </div>
    );
}

export default Player;