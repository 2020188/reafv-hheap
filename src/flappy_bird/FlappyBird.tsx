import React, { useEffect } from 'react';
import GameEngine from '../GameEngine/core/GameEngine';
import { Collision } from '../GameEngine/systems/Collision';
import { Physics } from '../GameEngine/systems/Physics';
import FlappyBirdEntities from './FlappyBirdEntities';
import { CollisionResolver } from './systems/CollisionResolver';
import { LoopBackSystem } from './systems/LoopBackSystem';
import { PipeSystem } from './systems/PipeSystem';
import { PointSystem } from './systems/PointSystem';
import { UpdatePosition } from './systems/UpdatePosition';
import { InputSystem } from './systems/InputSystem';


function FlappyBird() {

    return (
        <GameEngine entities={FlappyBirdEntities()} systems={[InputSystem, Physics, PipeSystem, LoopBackSystem, Collision, PointSystem, CollisionResolver, UpdatePosition]} running={true}>
        </GameEngine>
    )
}


export default FlappyBird