import { Vector2 } from "../GameEngine/utils/Vector2";
import { Pipe } from "../flappy_bird/entities/Pipe";
import { Background } from "../flappy_bird/entities/Background";

import { Floor } from "../flappy_bird/entities/Floor";
import { Bird } from "../flappy_bird/entities/Bird";
import { Point_box } from "../flappy_bird/entities/Point_box";
import { Frame } from "../flappy_bird/entities/Frame";
export default () => {

    const bird = Bird("Bird", new Vector2(170, 450));

    const pipe1u = Pipe("pipe1u", new Vector2(420, 0), true);
    const pipe1d = Pipe("pipe1d", new Vector2(420, 400), false);
    const point1 = Point_box("point1", new Vector2(420, 800));
    
    const pipe2u = Pipe("pipe2u", new Vector2(646, 0), true);
    const pipe2d = Pipe("pipe2d", new Vector2(646, 400), false);
    const point2 = Point_box("point2", new Vector2(646, 800));
    
    const bg = Background("background", new Vector2(0, 150));
   
    const floor = Floor("Floor", new Vector2(0, 700));
    
    const frameup = Frame("frameU", new Vector2(-54, -60), new Vector2(752, 210));
    const frameleft = Frame("frameL", new Vector2(-54, 150), new Vector2(54, 650));
    const framedown = Frame("frameD", new Vector2(-54, 800), new Vector2(752, 220));
    const frameright = Frame("frameR", new Vector2(400, 150), new Vector2(298, 650));
    
    return [bird, pipe1u, pipe1d, pipe2u, pipe2d, point1, point2, bg, floor, frameup, frameleft, framedown, frameright];
}