import { GameComponent } from "../../GameEngine/components/GameComponent";
import { Vector2 } from "../../GameEngine/utils/Vector2";

/**
 * Class defines a velocity vector.
 */
export class LoopBackComponent extends GameComponent {
	
	public looped : boolean;
	
	/**
	 * Constructs a new velocity component.
	 * @param vector - Vector2
	 */
	public constructor(public x:number, public loopBackPosition:Vector2) {
		super();
		this.looped = true;
	}
}