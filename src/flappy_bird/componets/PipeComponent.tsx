import { GameComponent } from "../../GameEngine/components/GameComponent";

export class PipeComponent extends GameComponent {
	/**
	 * Constructs a new velocity component.
	 * @param vector - Vector2
	 */
	public constructor(public upsideDown: boolean | null) {
		super();
	}
}