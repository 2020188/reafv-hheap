import { GameComponent } from "../../GameEngine/components/GameComponent";

export class PointComponent extends GameComponent {
	/**
	 * Constructs a new velocity component.
	 * @param vector - Vector2
	 */
	public constructor(public reached: boolean = false) {
		super();
	}
}