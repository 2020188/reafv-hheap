import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { GameComponent } from "../../GameEngine/components/GameComponent";
import { SpriteComponent } from "../../GameEngine/components/SpriteComponent";
import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { useReactEntity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import bgImg from '../../assets/flappyImage/background-day.png'//TODO: resource manager

export const Background = (name: string, position: Vector2) => {

	const width = 400;
	const height = 700;
	const scale = 1;

	const backgroundComponents: GameComponent[] = [
		new SpriteComponent(bgImg, width * scale, height * scale, 0),
	];

	const background = useReactEntity({ name: name, transform: { position }, components: backgroundComponents })
	return background;
}