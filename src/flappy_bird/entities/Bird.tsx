import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { GameComponent } from "../../GameEngine/components/GameComponent";
import { SpriteComponent } from "../../GameEngine/components/SpriteComponent";
import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { useReactEntity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import birdImg from '../../assets/flappyImage/yellowbird-midflap.png'//TODO: resource manager
import { PhysicsComponent } from "../../GameEngine/components/PhysicsComponent";

export const Bird = (name: string, position: Vector2) => {

	const birdSpeed = new Vector2(0, 0);
	const width = 50;
	const height = 35;
	const scale = 1;

	const gravity = new Vector2(0, 0.4);
	const mass = 0.925;
	const drag = 1;

	const birdComponents: GameComponent[] = [
		new SpriteComponent(birdImg, width * scale, height * scale, 3),
		new CollisionComponent(width * scale, height * scale),
		new VelocityComponent(birdSpeed),
		new PhysicsComponent(mass, drag, gravity)
	];

	const bird = useReactEntity({ name: name, transform: { position }, components: birdComponents })
	return bird;
}