import { GameComponent } from "../../GameEngine/components/GameComponent";
import { SpriteComponent } from "../../GameEngine/components/SpriteComponent";
import { useReactEntity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import frameImg from '../../assets/flappyImage/black.png'//TODO: resource manager


export const Frame = (name: string, position: Vector2, dimensions: Vector2) => {

	const width = dimensions.x;
	const height = dimensions.y;
	const scale = 1;

	const frameComponents: GameComponent[] = [
		new SpriteComponent(frameImg, width * scale, height * scale, 2)
	];

	const frame = useReactEntity({ name: name, transform: { position }, components: frameComponents })
	return frame;
}