import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { GameComponent } from "../../GameEngine/components/GameComponent";
import { SpriteComponent } from "../../GameEngine/components/SpriteComponent";
import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { useReactEntity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import pipeImg from '../../assets/flappyImage/pipe-green.png'//TODO: resource manager
import { LoopBackComponent } from "../componets/LoopComponent";
import { PipeComponent } from "../componets/PipeComponent";

export const Pipe = (name: string, position: Vector2, upsideDown : boolean) => {
	
	const pipeSpeed = new Vector2(-1,0);
	const width = 52;
	const height = 320;
	const scale = 1;
	const rotation = upsideDown ? 180 : 0;
	
	const pipeComponents: GameComponent[] = [
		new SpriteComponent(pipeImg, width * scale, height * scale, 1),
		new CollisionComponent(width * scale, height * scale),
		new VelocityComponent(pipeSpeed),
		new LoopBackComponent(-width, new Vector2(400, position.y)),
		new PipeComponent(upsideDown)
	];
	
	const pipe = useReactEntity({ name: name, transform: { position, rotation }, components: pipeComponents})
	return pipe;
	

}