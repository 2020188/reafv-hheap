
import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { GameComponent } from "../../GameEngine/components/GameComponent";
import { SpriteComponent } from "../../GameEngine/components/SpriteComponent";
import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { useReactEntity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import { LoopBackComponent } from "../componets/LoopComponent";
import { PipeComponent } from "../componets/PipeComponent";
import { PointComponent } from "../componets/PointComponent";

export const Point_box = (name: string, position: Vector2) => {

	const pipeSpeed = new Vector2(-1, 0);
	const width = 52;
	const height = 230;
	const scale = 1;

	const pointComponents: GameComponent[] = [
		new CollisionComponent(width * scale, height * scale),
		new VelocityComponent(pipeSpeed),
		new LoopBackComponent(-width, new Vector2(400, position.y)),
		new PipeComponent(null),
		new PointComponent()
	];
	
	const points = useReactEntity({name: name, transform: { position }, components: pointComponents })
	return points;
}