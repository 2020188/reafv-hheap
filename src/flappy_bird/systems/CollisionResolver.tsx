import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { all } from "../../GameEngine/utils";
import { Vector2 } from "../../GameEngine/utils/Vector2";

export const CollisionResolver = (entities: Entity[]) => {
	const bird = entities.find(entity => entity.name == "Bird");
	
	const colliders = bird!.getComponent(CollisionComponent)!.collides
	const times = bird!.getComponent(CollisionComponent)!.time;
	
	let minTime = 1;
	
	while (colliders.length != 0) {
		
		const collider = colliders.pop();
		const time = times.pop();
		if (collider!.getParent()!.name == "point1") continue;
		if (collider!.getParent()!.name == "point2") continue;
		if (time! < minTime) minTime = time!;
	}
	// const velocity = bird!.getComponent(VelocityComponent)!;
	// velocity.setVector(new Vector2(velocity.vector.x * minTime, velocity.vector.y * minTime));
	
	if (minTime < 1) all(entities, [VelocityComponent]).forEach(entity =>
		entity.getComponent(VelocityComponent)!.setVector(new Vector2(0, 0))
	);
	
	all(entities, [CollisionComponent]).forEach(entity =>
		entity.getComponent(CollisionComponent)!.collides = []
	);
	
	return entities;
}