import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core";
import { Vector2 } from "../../GameEngine/utils/Vector2";


export const InputSystem = (entities: Entity[], args: any) => {
    const { payload } = args.input.find((x: any) => x.name === "onMouseDown") || args.input.find((x: any) => x.name === "onKeyDown") || {};
   // const keyEvent: KeyboardEvent = args.input.find((x: any) => x.name === "onKeyDown") || null;
    
    if (payload?.button === 0 || payload?.which == "a") {
        const obj1 = entities.find((entity) => entity.name == "Bird")!;
    const currentRot = obj1.transform.rotation;
    obj1.transform.setRotation(currentRot > 15 ? -15 : -40);
    //obj1.transform.setPosition(new Vector2(payload.pageX, payload.pageY));
    obj1.getComponent(VelocityComponent)?.setVector(new Vector2(0,-12));
    }

    return entities;
}