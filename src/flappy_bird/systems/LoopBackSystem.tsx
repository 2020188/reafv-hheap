import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { all } from "../../GameEngine/utils";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import { LoopBackComponent } from "../componets/LoopComponent";

export const LoopBackSystem = (entities: Entity[]) => {
	
	const loopComp = all(entities, [LoopBackComponent])
	
	loopComp.forEach(loopComp => {
		const comp = loopComp.getComponent(LoopBackComponent);
		
		if (comp!.getParent()!.transform.getPosX() < comp!.x) {
			comp!.getParent()!.transform.setPosition(new Vector2(comp!.loopBackPosition.x, comp!.loopBackPosition.y));
			comp!.looped = true;
		}
	});
	
	return entities;
}