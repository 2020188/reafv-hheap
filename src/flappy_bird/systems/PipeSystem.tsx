import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { all } from "../../GameEngine/utils";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import { LoopBackComponent } from "../componets/LoopComponent";
import { PipeComponent } from "../componets/PipeComponent";

export const PipeSystem = (entities: Entity[]) => {
	const random = Math.random();
	const pipes = all(entities, [PipeComponent, LoopBackComponent])
	
	pipes.forEach(pipe => {
		const pipeComp = pipe.getComponent(PipeComponent);
		const loopBackComp = pipe.getComponent(LoopBackComponent);
		
		if (!loopBackComp!.looped) return;
		
		
		pipeComp!.getParent()!.transform.setPosition(
			new Vector2(
				pipeComp!.getParent()!.transform.getPosX(),
				pipeComp!.upsideDown == null ? random * 320 + 150 :
				pipeComp!.upsideDown ? random * 320 - 170 : random * 320 + 380
			)
		);
		loopBackComp!.looped = false;
	});
	return entities;
}