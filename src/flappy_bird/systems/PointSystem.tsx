import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { SpriteComponent } from "../../GameEngine/components/SpriteComponent";
import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { all } from "../../GameEngine/utils";
import { LoopBackComponent } from "../componets/LoopComponent";
import { PointComponent } from "../componets/PointComponent";
import backgroundDay from "../../assets/flappyImage/background-day.png";
import backgroundNight from "../../assets/flappyImage/background-night.png";
import greenPipe from "../../assets/flappyImage/pipe-green.png";
import redPipe from "../../assets/flappyImage/pipe-red.png";

let point = 0;
const day = backgroundDay;
const night = backgroundNight;
const green = greenPipe;
const red = redPipe;

export const PointSystem = (entities: Entity[]) => {
	
	const pointBoxes = all(entities, [PointComponent])
	pointBoxes.forEach(pointBox => {
		const pointComp = pointBox.getComponent(PointComponent);
		const collisionComp = pointBox.getComponent(CollisionComponent);
		const loopBackComponent = pointBox.getComponent(LoopBackComponent);
		
		if (loopBackComponent!.looped) pointComp!.reached = false;
		if (pointComp!.reached) return;
		
		const bird = entities.find(entity => entity.name == "Bird")
		
		
		if (collisionComp!.collides.includes(bird!.getComponent(CollisionComponent)!)) {
			console.log(++point);
			pointComp!.reached = true;
		}
	});
	
	const background = entities.find(entity => entity.name == "background")!.getComponent(SpriteComponent)!;
	const pipe1u = entities.find(entity => entity.name == "pipe1u")!.getComponent(SpriteComponent)!;
	const pipe1d = entities.find(entity => entity.name == "pipe1d")!.getComponent(SpriteComponent)!;
	const pipe2u = entities.find(entity => entity.name == "pipe2u")!.getComponent(SpriteComponent)!;
	const pipe2d = entities.find(entity => entity.name == "pipe2d")!.getComponent(SpriteComponent)!;
	
	levelUp(0, background!, day);
	levelUp(10, background!, night);
	levelUp(20, pipe2u, redPipe)
	levelUp(20, pipe2d, redPipe)
	levelUp(20, background!, day);
	levelUp(20, pipe1u, redPipe)
	levelUp(20, pipe1d, redPipe)
	levelUp(30, background!, night);
	
	return entities;
}

const levelUp = (count : number, sprite : SpriteComponent, link: string) => {
	if (point != count) return;
	
	sprite.link = link;
}