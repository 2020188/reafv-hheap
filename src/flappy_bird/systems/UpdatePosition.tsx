import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { all } from "../../GameEngine/utils";
import { Vector2 } from "../../GameEngine/utils/Vector2";

export const UpdatePosition = (entities: Entity[]) => {
    const velocityEntities = all(entities, [VelocityComponent]);
	
    const bird = entities.find(entity => entity.name == "Bird")!;
    const bTrans = bird.transform;
    const birdVel = bird.getComponent(VelocityComponent)!;
    if(birdVel.vector.y > 2){
        bTrans.rotate(birdVel.vector.y / 2.3);
    }
	
    velocityEntities.forEach((entity) => {
        const velComp = entity.getComponent(VelocityComponent);
		entity.transform.translate(velComp!.vector);
    })
    return entities;
}
