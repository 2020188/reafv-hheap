import React, { useEffect } from 'react';
import GameEngine from "../GameEngine/core/GameEngine";
import Game2Entities from "./Game2Entities";
import { Physics } from "../GameEngine/systems/Physics";
import { SimpleCollision } from "../GameEngine/systems/SimpleCollision";
import { CameraSystem } from "./systems/CameraSystem";
import { CollisionResolution } from "./systems/CollisionResolution";
import { GameController } from "./systems/GameController";
import { InputSystem } from "./systems/InputSystem";
import { WrapAroundSystem } from "./systems/WrapAroundSystem";
import { PlatformSystem } from "./systems/PlatformSystem";
import { VelocitySystem } from './systems/VelocitySystem';

function Game2() {

	return (		
				<GameEngine entities={Game2Entities()} systems={[GameController, InputSystem, CollisionResolution, Physics, SimpleCollision, CameraSystem, PlatformSystem, VelocitySystem, WrapAroundSystem]}>
				</GameEngine>
	)
}


export default Game2;