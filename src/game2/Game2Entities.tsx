import { Vector2 } from "../GameEngine/utils/Vector2";

import { Jumper } from "./entities/Jumper";
import { Platform } from "./entities/Platform";
import { Background } from "./entities/Background";
import { Floor } from "./entities/Floor";
import { ScoreUI } from "./entities/ScoreUI";

export default () => {
    const jumper = Jumper("Jumper", new Vector2(170, 450));
    
    const bg = Background("Background", new Vector2(0, 150));
   
    const floor = Floor("Floor", new Vector2(0, 700));

    const platform1 = Platform("Platform1", new Vector2(50, 500));
    const platform2 = Platform("Platform2", new Vector2(200, 300));
    const platform3 = Platform("Platform3", new Vector2(150, 150));
    const platform4 = Platform("Platform4", new Vector2(300, 50));
    const platform5 = Platform("Platform5", new Vector2(100, -100));

    const scoreUI = ScoreUI("ScoreUI", new Vector2(200, 150));
    
    return [jumper, bg, floor, platform1, platform2, platform3, platform4, platform5, scoreUI ];
}