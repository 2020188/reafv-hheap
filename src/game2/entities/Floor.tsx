import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { GameComponent } from "../../GameEngine/components/GameComponent";
import { SpriteComponent } from "../../GameEngine/components/SpriteComponent";
import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { useReactEntity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import floorImg from '../../assets/flappyImage/base.png'//TODO: resource manager

export const Floor = (name: string, position: Vector2) => {

	const floorSpeed = new Vector2(-1, 0);
	const width = 430;
	const height = 100;
	const scale = 1;

	const floorComponents: GameComponent[] = [
		new SpriteComponent(floorImg, width * scale, height * scale, 1),
		new CollisionComponent(width * scale, height * scale),
	];

	const floor = useReactEntity({ name: name, transform: { position }, components: floorComponents })
	return floor;
}