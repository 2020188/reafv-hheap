import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { GameComponent } from "../../GameEngine/components/GameComponent";
import { SpriteComponent } from "../../GameEngine/components/SpriteComponent";
import { useReactEntity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import platformImg from '../../assets/flappyImage/base.png'//TODO: resource manager

export const Platform = (name: string, position: Vector2) => {

	const width = 430;
	const height = 100;
	const scale = 0.15;

	const platformComponents: GameComponent[] = [
		new SpriteComponent(platformImg, width * scale, height * scale, 1),
		new CollisionComponent(width * scale, height * scale),
	];

	const platform = useReactEntity({ name: name, transform: { position }, components: platformComponents })
	return platform;
}