import { GameComponent } from "../../GameEngine/components/GameComponent";
import { UITextComponent } from "../../GameEngine/components/UITextComponent";
import { useReactEntity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";

export const ScoreUI = (name: string, position: Vector2) => {

	const scoreComponents: GameComponent[] = [
		new UITextComponent("Score: ")
	];

	const scoreUI = useReactEntity({ name: name, transform: { position }, components: scoreComponents })
	return scoreUI;
}