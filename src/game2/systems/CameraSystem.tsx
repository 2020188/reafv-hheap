import { UITextComponent } from "../../GameEngine/components/UITextComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";

let score = 0;

export const CameraSystem = (entities: Entity[], args: any) => {
    const jumper = entities.find((enitity)=> enitity.name == "Jumper")!;
    const jumperYPos = jumper.transform.getPosY();

    const scoreUI = entities.find(entity => entity.name == "ScoreUI")!.getComponent(UITextComponent)!;
    

    // y-coordinate threshold for when the camera should move.
    const threshold = 250; 

    if(args.events.find((event: Event) => event.type == "Game Over")){
        scoreUI.text = "Game over";
    }

    if(jumperYPos < threshold){
        const thresholdDelta = threshold - jumperYPos;
        //Moves all entities except background by the amount that the player is above the threshold
        entities.forEach((entity) => {
            if(entity.name == "Background" || entity.name == "ScoreUI"){
                return;
            }
            entity.transform.translate(new Vector2(0, thresholdDelta));
        })
        
        score += thresholdDelta
        //console.log("Score: " + score);
        scoreUI.text = "Score: " + Math.floor(score);
    }
    return entities
}