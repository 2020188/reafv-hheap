import { CollisionComponent } from "../../GameEngine/components/CollisionComponent";
import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";

export const CollisionResolution = (entities: Entity[], args: any) => {
    const jumper = entities.find((enitity)=> enitity.name == "Jumper")!;
    const jumperColl = jumper.getComponent(CollisionComponent)!;
    const jumperVel = jumper.getComponent(VelocityComponent)!;
    
    //if jumper collides and is falling then make him jump
    if(jumperColl.collides.length > 0 && jumperVel.vector.y >= 0){
        jumperVel.addVector(new Vector2(0,-20));
        jumperColl.collides! = [];
    }
    return entities;
}