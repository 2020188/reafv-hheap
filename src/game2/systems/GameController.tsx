import { Entity } from "../../GameEngine/core/Entity";

let gameOver = false;
let score: number; 

export const GameController = (entities: Entity[], args: any) => {
    const floor = entities.find((enitity)=> enitity.name == "Floor");
    const jumper = entities.find((enitity)=> enitity.name == "Jumper");

    //if floor is out of frame then remove it
    if(floor){
        if(floor.transform.getPosY() > 820){
            entities = entities.filter((enitity)=> enitity.name != "Floor");
        }
    }

    if(!gameOver && jumper!.transform.getPosY() > 830){
        //TODO: Enter some lose state.
        args.dispatchEvent(new Event("Game Over"));
        gameOver = true;
    }

    return entities;
}