import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";

enum keyBinds{
    Left = "a",
    Right = "d"
}

type keyEvent = {
    name: string,
    payload: KeyboardEvent
}

export const InputSystem = (entities: Entity[], args: any) => {
    //check if a onKeyPress or onKeyUp event happend
    const keyPress: keyEvent = args.input.find((x: any) => x.name === "onKeyPress");
    const keyUp: keyEvent = args.input.find((x: any) => x.name === "onKeyUp");
    
    const jumper = entities.find((entity) => entity.name == "Jumper")!;
    const JumperVel = jumper.getComponent(VelocityComponent)!;

    if (keyUp) {
        //If player stopped pressing a key then set jumper x velocity to 0
        if (keyUp.payload.key == keyBinds.Left || keyUp.payload.key == keyBinds.Right) {
            JumperVel.setVector(new Vector2(0, JumperVel.vector.y));
        }
    }
    
    if (keyPress) {
        if (keyPress.payload.key == keyBinds.Left) {
            JumperVel.setX(-2);
        }
        if (keyPress.payload.key == keyBinds.Right) {
            JumperVel.setX(2);
        }
    }
    

    return entities;
};