import { Entity } from "../../GameEngine/core/Entity";
import { Vector2 } from "../../GameEngine/utils/Vector2";
import { Platform } from "../entities/Platform";

export const PlatformSystem = (entities: Entity[], args: any) => {
    //TODO: ger hetta til ein util
    const platforms = entities.filter((enitity) => enitity.name.includes("Platform"));

    platforms.forEach((platform)=>{
        //if platform is out of frame then move it to the top of the screen at a random location
        if(platform.transform.getPosY() > 900){
            const randomX = Math.random() * 400
            platform.transform.setPosition(new Vector2(randomX, 120))
        }
    })

    return entities;
}