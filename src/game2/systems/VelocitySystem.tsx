import { VelocityComponent } from "../../GameEngine/components/VelocityComponent";
import { Entity } from "../../GameEngine/core/Entity";
import { all } from "../../GameEngine/utils";

//translates the position of all entities by their velocity
export const VelocitySystem = (entities: Entity[]) => {
    const velocityEntities = all(entities, [VelocityComponent]);
	
    velocityEntities.forEach((entity) => {
        const velComp = entity.getComponent(VelocityComponent);
		entity.transform.translate(velComp!.vector);
    })
    return entities;
}