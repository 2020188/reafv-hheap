import { Entity } from "../../GameEngine/core/Entity"
import { Vector2 } from "../../GameEngine/utils/Vector2";

//make jumper wrap around the screen
export const WrapAroundSystem = (entities: Entity[], args: any) => {
    const jumper = entities.find((enitity)=> enitity.name == "Jumper")!;
    const xPos = jumper.transform.getPosX();
    const yPos = jumper.transform.getPosY();

    if(xPos < -20){
        jumper.transform.setPosition(new Vector2(400, yPos));
    }
    if(xPos > 410){
        jumper.transform.setPosition(new Vector2(0, yPos));
    }
    return entities;
}