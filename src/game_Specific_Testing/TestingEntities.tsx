import { Vector2 } from "../GameEngine/utils/Vector2";
import { Pipe } from "../flappy_bird/entities/Pipe";
import { Background } from "../flappy_bird/entities/Background";

import { Floor } from "../flappy_bird/entities/Floor";
import { Bird } from "../flappy_bird/entities/Bird";
import { Point_box } from "../flappy_bird/entities/Point_box";
import { Frame } from "../flappy_bird/entities/Frame";

//export function useTestObjects() {
export default () => {

    // const birdComps: GameComponent[] = [new SpriteComponent(birdImg, 50, 35, 3), new CollisionComponent(50, 35), new VelocityComponent(new Vector2(0,0)), new PhysicsComponent(0.4, 1)];
    // const bird: Entity = ReactEntity({x: 170, y: 550, components: birdComps});
    
    const bird = Bird("Bird", new Vector2(170, 450));

    // const pipeComps: GameComponent[] = [new SpriteComponent(pipeImg, 60, 300, 2), new CollisionComponent(60, 300), new VelocityComponent(new Vector2(-1, 0))];
    // const pipe: Entity = ReactEntity({x: 300, y: 400, components: pipeComps})
    const pipe1u = Pipe("pipe1u", new Vector2(420, 0), true);
    const pipe1d = Pipe("pipe1d", new Vector2(420, 400), false);
    const point1 = Point_box("point1", new Vector2(420, 800));
    
    const pipe2u = Pipe("pipe2u", new Vector2(646, 0), true);
    const pipe2d = Pipe("pipe2d", new Vector2(646, 400), false);
    const point2 = Point_box("point2", new Vector2(646, 800));

    // const bgComps: GameComponent[] = [new SpriteComponent(bgImg, 400, 700, 0)];
    // const bg: Entity = ReactEntity({x: 0, y: 150, components: bgComps});
    
    const bg = Background("background", new Vector2(0, 150));

    // const floorComps: GameComponent[] = [new SpriteComponent(floorImg, 400, 100, 1), new CollisionComponent(400, 100)];
    // const floor: Entity = ReactEntity({x: 0, y: 700, components: floorComps});
   
    const floor = Floor("Floor", new Vector2(0, 700));
    
    const frameup = Frame("frameU", new Vector2(-54, -60), new Vector2(752, 210));
    const frameleft = Frame("frameL", new Vector2(-54, 150), new Vector2(54, 650));
    const framedown = Frame("frameD", new Vector2(-54, 800), new Vector2(752, 220));
    const frameright = Frame("frameR", new Vector2(400, 150), new Vector2(298, 650));
    
    return [bird, pipe1u, pipe1d, pipe2u, pipe2d, point1, point2, bg, floor, frameup, frameleft, framedown, frameright];
}