import { VelocityComponent } from "../GameEngine/components/VelocityComponent";
import { Entity } from "../GameEngine/core/Entity";
import { Vector2 } from "../GameEngine/utils/Vector2";

export default (entities: Entity[], args: any) => {
    const { payload } = args.input.find((x: any) => x.name === "onMouseDown") || {};

  if (payload) {
    
    const obj1 = entities[0];
    const currentRot = obj1.transform.rotation;
    obj1.transform.setRotation(currentRot > 15 ? -15 : -40);
    //obj1.transform.setPosition(new Vector2(payload.pageX, payload.pageY));
    obj1.getComponent(VelocityComponent)?.setVector(new Vector2(0,-8));
  }
  
  return entities;
};


// export default (entities: {}, args: any) => {
//   const { payload } = args.input.find((x: any) => x.name === "onMouseDown") || {};

// if (payload) {
  
//   const obj1 = (entities as any)["testObject1"];
//   console.log(obj1);
//   obj1.x = payload.pageX;
//   obj1.y = payload.pageY;
//   console.log(obj1);
// }

// return entities;
// };